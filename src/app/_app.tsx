import App, { AppContext, AppInitialProps, AppProps } from "next/app";

type AppOwnProps = { example: any };

export default function MyApp({
  Component,
  example,
  pageProps,
}: AppProps & AppOwnProps) {
  return (
    <>
      <p>Example: {example}</p>
      <Component {...pageProps} />
    </>
  );
}

MyApp.getInitialProps = async (
  context: AppContext
): Promise<AppOwnProps & AppInitialProps> => {
  const ctx = await App.getInitialProps(context);

  return { ...ctx, example: "data" };
};
